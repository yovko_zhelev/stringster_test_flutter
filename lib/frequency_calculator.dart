import 'dart:math';

import 'package:flutter/cupertino.dart';

import 'fft/fft_algorithm.dart';
import 'fft/window.dart';

class FrequencyCalculator {

  List<int> data;

  FrequencyCalculator();

  double calculate({@required dynamic samples, @required int sampleRate}) {
    try {
      data =  List<int>.from(samples);
    } catch (e) {
      print(e);
    }

    data = data.sublist(0, highestPowerOf2(data.length));

    final Iterable<num> windowed = Window(WindowType.HANN).apply(data);
    final List<Complex> fft = FFT().Transform(windowed);
    final List<num> magnitude = data.sublist(0, data.length ~/ 2);

    for (int i = 0; i <= data.length / 2 - 1; i++) {
      final num real = fft[i].real;
      final num imag = fft[i].imaginary;
      magnitude[i] = sqrt(real * real + imag * imag).toInt();
    }

    num maxMagnitude = -double.infinity;
    int maxIndex = -1;

    for (int i = 0; i <= data.length / 2 - 1; i++) {
      if (magnitude[i] > maxMagnitude) {
        maxMagnitude = magnitude[i];
        maxIndex = i;
      }
    }

    return maxIndex * sampleRate / data.length;
  }

  double getRMS() {
    double var1 = 0.0;

    for (int var3 = 0; var3 < data.length; ++var3) {
      var1 += (data[var3] * data[var3]).toDouble();
    }

    var1 /= data.length.toDouble();
    var1 = sqrt(var1);
    return var1;
  }

  int highestPowerOf2(int n) {
    int res = 0;

    for (int i = n; i >= 1; i--) {
      //If i is a power of 2
      if ((i & (i - 1)) == 0) {
        res = i;
        break;
      }
    }

    return res;
  }
}