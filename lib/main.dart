import 'dart:async';

import 'package:stringster_test_flutter/frequency_calculator.dart';
import 'package:stringster_test_flutter/models/waveform_data_model.dart';
import 'package:stringster_test_flutter/ui/widgets/painted_waveform.dart';
import 'package:flutter/material.dart';

import 'package:audio_recorder_mc/audio_recorder_mc.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  GlobalKey<PaintedWaveformState> waveFormKey = GlobalKey();

  Timer timer;

  final FrequencyCalculator _frequencyCalculator = FrequencyCalculator();

  final int sampleRate = 96000;

  final int minTensionFrequency = 760;
  final int maxTensionFrequency = 1525;

  AudioRecorderMc recorder;

  WaveformData waveformData;

  double frequency = -1.0;

  bool isRecording = false;


  @override
  void initState() {
    super.initState();

    recorder = AudioRecorderMc()..setup(sampleRate: sampleRate, sampleFormat: AudioRecorderFormat.pcm16);

    timer = Timer.periodic(Duration(milliseconds: 100), (timer) {

    });

    startRecord();
  }

  void startRecord() {
    recorder.startRecord.then((stream) {
      setState(() {
        isRecording = true;
      });

      stream.listen((dynamic samples) {
        final double freq = _frequencyCalculator.calculate(samples: samples, sampleRate: sampleRate);

        if (freq > minTensionFrequency && freq < maxTensionFrequency) {
          frequency = freq;

          print("FREQ -> $frequency");
        }

        waveformData = WaveformData(data: _frequencyCalculator.data, dataRms: _frequencyCalculator.getRMS());

        waveFormKey?.currentState?.mainPainter?.data = waveformData;
        waveFormKey?.currentState?.rmsPainter?.data = waveformData;

      });
    });
  }

  void stopRecord() {
    recorder.stopRecord.then((value) {
      setState(() {
        isRecording = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text("Frequency: $frequency Hz"),
        ),
        body: body(),
      ),
    );
  }

  Widget body() {
    return PaintedWaveform(key: waveFormKey);
  }
}