import 'dart:math';
import 'dart:ui';
import 'dart:core';

import 'package:flutter/animation.dart';
import 'package:vector_math/vector_math_64.dart' as math;
import 'package:flutter/painting.dart';

class WaveformData {
  List<int> data;
  double dataRms;
  int preferredNumberCirclePointsToDraw = 240;

  final int maxRangeFor16bitNums = 32768;

  WaveformData({
    this.data,
    this.dataRms,
  });

  Path path(Size size, {AnimationController controller, double radiusScale, double pitchScale}) {
    return getCircleWaveFormPath(data, size, controller, radiusScale, pitchScale);
  }

  double normalizeRMS(double rms, double pitchScale) {
    double max = 20000.0 * pitchScale;
    double min = 0.0;

    double currVal = rms > max ? max : rms;
    double normalized = (currVal - min) / (max - min) + 0.5;
    print("Normalised RMS " + (normalized - 0.5).toString());
    return normalized;
  }

  Path getCircleWaveFormPath(List<int> audioData, size, AnimationController controller, double radiusScale,
      double pitchScale) {
    List<double> data = audioData.map((e) => e.toDouble() / maxRangeFor16bitNums).toList();
    // ------------------ iOS implementation ------------------------
    Path path = Path();

    double halfWidth = size.width / 2.0;
    double halfHeight = size.height / 2.0;

    double centerX = halfWidth;
    double centerY = halfHeight;

    double normalizedRms = normalizeRMS(dataRms, pitchScale);
    double radius = min(halfWidth, halfHeight) * normalizedRms;
    print("RMS " + normalizedRms.toString());

    double maxPitch = radius / radiusScale;

    int dataSliceSize = data.length ~/ (preferredNumberCirclePointsToDraw * (pitchScale == 1.0 ? 1 : 2.5));
    int numberOfSteps = data.length ~/ dataSliceSize;
    double angleStep = 360 / numberOfSteps;

    double angle = 0.0;
    double avg = 0.0;

    Offset startingPoint;
    Offset prevPoint;

    for (int i = 1; i < data.length; i++) {
      double curVal = data[i];
      avg = (avg + (min(curVal, radius)).abs());

      if (i % dataSliceSize == 0) {
        angle = angle + angleStep;
        avg = avg / dataSliceSize;

        double newX = centerX +
            (radius + (avg * maxPitch)) * cos(math.radians(angle + (pitchScale == 1.0 ? 0 : 20)));
        double newY = centerY +
            (radius + (avg * maxPitch)) * sin(math.radians(angle + (pitchScale == 1.0 ? 0 : 20)));

        path.moveTo(newX, newY);

        if (prevPoint != null) {
          path.lineTo(prevPoint.dx, prevPoint.dy);
        } else {
          startingPoint = Offset(newX, newY);
        }

        prevPoint = Offset(newX, newY);
      }
    }


    path.moveTo(prevPoint.dx, prevPoint.dy);
    path.lineTo(startingPoint.dx, startingPoint.dy);

    path.close();
    return path;
  }
}