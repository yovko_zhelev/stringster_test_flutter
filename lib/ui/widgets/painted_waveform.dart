import 'package:flutter/material.dart';
import 'package:stringster_test_flutter/models/waveform_data_model.dart';
import 'package:stringster_test_flutter/ui/widgets/waveform_painter.dart';

class PaintedWaveform extends StatefulWidget {
  PaintedWaveform({
    Key key,
  }) : super(key: key);

  @override
  PaintedWaveformState createState() => PaintedWaveformState();
}

class PaintedWaveformState extends State<PaintedWaveform> with SingleTickerProviderStateMixin {
  AnimationController _controller;
  WaveformData sampleData;
  WaveformPainter mainPainter;
  WaveformPainter rmsPainter;

  @override
  void initState() {
    _controller = AnimationController(vsync: this, duration: Duration(milliseconds: 100), animationBehavior: AnimationBehavior.preserve);
    _controller.repeat();

    mainPainter = WaveformPainter(
      sampleData,
      strokeWidth: 1.5,
      listenable: _controller,
    );

    rmsPainter = WaveformPainter(
        sampleData,
        radiusScale: 0.4,
        pitchScale: 0.3,
        strokeWidth: 0.4,
        listenable: _controller,
        color: Colors.lightBlueAccent
    );

    super.initState();
  }

  @override
  void dispose(){
    _controller.dispose();

    super.dispose();
  }

  @override
  Widget build(context) {
    _controller.duration = Duration(seconds: 1);
    return Container(
      color: Colors.black,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          LayoutBuilder(
            builder: (context, BoxConstraints constraints) {
              // adjust the shape based on parent's orientation/shape
              // the waveform should always be wider than taller
              var height;
              if (constraints.maxWidth < constraints.maxHeight) {
                height = constraints.maxWidth;
              } else {
                height = constraints.maxHeight;
              }

              return AnimatedBuilder(
                animation: _controller,
                builder: (_, __) => Container(
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Image.asset("assets/stringster-logo-white@3x.png", width: 70, height: 70,),

                      CustomPaint(
                        size: Size(
                          constraints.maxWidth,
                          height,
                        ),
                        foregroundPainter: rmsPainter,
                      ),

                      CustomPaint(
                        size: Size(
                          constraints.maxWidth,
                          height,
                        ),
                        foregroundPainter: mainPainter,
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
