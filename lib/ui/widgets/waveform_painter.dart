import 'package:flutter/material.dart';
import 'dart:ui';
import 'package:stringster_test_flutter/models/waveform_data_model.dart';

class WaveformPainter extends CustomPainter {
  WaveformData data;
  final double radiusScale;
  Paint painter;
  final Color color;
  final double strokeWidth;
  final Animation listenable;
  final double pitchScale;

  WaveformPainter(this.data, {
    this.radiusScale = 0.07,
    this.strokeWidth = 1.0,
    this.color = Colors.redAccent,
    this.listenable,
    this.pitchScale = 1.0,
  }) : super(repaint: listenable) {
    painter = Paint()
      ..style = PaintingStyle.stroke
      ..color = color
      ..strokeWidth = strokeWidth
      ..strokeJoin = StrokeJoin.round
      ..strokeCap = StrokeCap.round
      ..isAntiAlias = true;
  }

  setVolumeDependentColor(double volume, Paint painter) {
    if (volume > 0.12) {
      painter.color = Color(0xfffec93c);
    } else if (volume > 0.05) {
      painter.color = Color(0xfffb8a40);
    } else if (volume > 0.01) {
      painter.color = Color(0xfff82848);
    } else if (volume > 0.001) {
      painter.color = Color(0xff3da1fa);
    } else {
      painter.color = Color(0xff0ae1ae);
    }
  }

  double normalizeRMS(double rms, double pitchScale) {
    print("Raw RMS " + rms.toString());
    double max = 32000.0 * pitchScale;
    double min = 0.0;

    double currVal = rms > max ? max : rms;
    return (currVal - min) / (max - min) + 0.5;
  }

  @override
  void paint(Canvas canvas, Size size) {
    if (data == null) {
      return;
    }

    final path = data.path(size, controller: listenable, radiusScale: this.radiusScale, pitchScale: this.pitchScale);

    canvas.drawPath(path, painter);
  }



  @override
  bool shouldRepaint(WaveformPainter oldDelegate) {
    if (data == null) {
      return false;
    }

    final double noralizedRms = data.normalizeRMS(data.dataRms, pitchScale) - 0.5;

    setVolumeDependentColor(noralizedRms, painter);
    Animation _colorTween = ColorTween(begin: oldDelegate.painter.color, end: painter.color).animate(listenable);

    print(listenable.value);

    painter.color = _colorTween.value;

    return listenable.value != oldDelegate.listenable.value;
  }
}
